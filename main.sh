#!/bin/sh

Line=100 				# Catch lines.
LogPath="/var/log/mail.log" 		# Log path.
ExceptionList="/mail/exlist.txt" 	# Exception list.

ErrorSASLTimes=3 			# For SASL
ErrorConnectionTimes=9 			# For connection.

DenyTime=1800 				# Unit is Second.
DenyLimit=3 				# Ever block.

dir="$(dirname "$0")"			# Dirctiory

. "$dir/ts_get_sec.sh"

###################################################################################################

SASL_IP=$(tail -n $Line $LogPath | grep -v -f $dir/lastlog.txt | grep SASL | grep -E -o  --color '([0-9]{1,3}\.){3}[0-9]{1,3}' | grep -v -f $ExceptionList | grep -v -f $dir/banned.txt )
CONNECTION_IP=$(tail -n $Line $LogPath | grep -v -f $dir/lastlog.txt | grep "lost connection" | grep -E -o  --color '([0-9]{1,3}\.){3}[0-9]{1,3}' | grep -v -f $ExceptionList | grep -v -f $dir/banned.txt )

LastLog=$(tail -n $Line $LogPath | awk '{print $3}' > $dir/lastlog.txt)

###################################################################################################

ban_ip=$(echo $SASL_IP | sort | uniq -c | awk '{if( $1 > $ErrorSASLTimes) print $2}')

for ip in $ban_ip
do
#	echo $ip fail to ban
	now_date=$(date +"%b %d %T")
	now_time=$(date +"%T")
	hostname=$(hostname)

	iptables -L -n | grep $ip &>/dev/null
	if [ "$?" -ne "0" ]; then
		iptables -I INPUT -s $ip -p tcp --dport 587 -j DROP -m comment --comment "$now_time SASL ban" || echo $now_date $hostname postfix/block[0010] : This ip has banned [$ip] >> $LogPath
		echo $now_date $hostname postfix/block[0001]: ban ip [$ip] >> $LogPath
		echo $ip 1 > $dir/count.txt
	fi
done

now_time=$(date +"%T")
ip_time=$(iptables -L -n | grep "SASL ban"| awk '{print $9}' | uniq -c | awk '{print $2}' )

for deny_time in $ip_time
do 
	START=$(ts_get_sec $deny_time)
	STOP=$(ts_get_sec $now_time)
	DIFF=$((STOP-START))
	echo $DIFF >> $dir/test1.txt
	if [ "$DIFF" -ge "$DenyTime" ]; then
		ip_line_number=$(iptables -L -n --line-number | grep $deny_time | grep "SASL ban" | awk '{print $1}')
		for line_number in $ip_line_number
		do
			iptables -D INPUT $line_number
		done
	fi
done

###################################################################################################

ban_ip2=$(echo $CONNECTION_IP | sort | uniq -c | awk '{if( $1 > $ErrorConnectionTimes) print $2}')
for ip in $ban_ip2
do
#	echo $ip connect to ban
	now_date=$(date +"%b %d %T")
	now_time=$(date +"%T")
	hostname=$(hostname)

	iptables -L -n | grep $ip &>/dev/null
	if [ "$?" -ne "0" ]; then
		iptables -I INPUT -s $ip -p tcp --dport 587 -j DROP -m comment --comment "$now_time connection ban"  || echo $now_date $hostname postfix/block[0010] : This ip has banned [$ip] >> $LogPath
		echo $now_date $hostname postfix/block[0002]: ban ip [$ip] >> $LogPath
		echo $ip 2 >> $dir/count.txt
	fi
done
	
now_time=$(date +"%T")
ip_time=$(iptables -L -n | grep "connection ban"| awk '{print $9}' | uniq -c | awk '{print $2}' )

for deny_time in $ip_time
do 
	START=$(ts_get_sec $deny_time)
	STOP=$(ts_get_sec $now_time)
	DIFF=$((STOP-START))
	if [ "$DIFF" -ge "$DenyTime" ]; then
		ip_line_number=$(iptables -L -n --line-number | grep $deny_time | grep "connection ban" | awk '{print $1}')
		for line_number in $ip_line_number
		do
			iptables -D INPUT $line_number
		done
	fi
done

###################################################################################################

deny_ip=$(cat $dir/count.txt | sort | uniq -c | awk '{if($1 > $DenyLimit) print $2}')

for ip in $deny_ip
do
	now_date=$(date +"%b %d %T")
	now_time=$(date +"%T")
	hostname=$(hostname)

	iptables -I INPUT -s $ip -p tcp --dport 587 -j DROP -m comment --comment "$now_time DENY"
	echo $now_date $hostname postfix/block[0003]: block ip [$ip] >> $LogPath
	ip_list=$(cat $dir/count.txt | grep -v $ip)
	echo $ip_list > $dir/count.txt
	echo $ip >> $dir/banned.txt
done
