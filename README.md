Introduction：
=======
This project analysis syslog to protect main service.

Try protect service from Brute-force attack and smail DoS.

Use iptables to block source ip address.

For Postfix and Dovecot.

You can try it on others mail services package or service.

You should edit variables for your environment.

Environment：
=======
OS: Debian 7.9.0 stable

Develop by vim.

Use Shell Script (sh)

How to Use：
==========
1. Download all files and move to same directory.

2. Edit variables of your environment in main.sh.

3. Run main.sh in background. (sh main.sh &)